job "metrics-consul" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  vault {
    policies = ["job-metrics-consul"]
  }

  group "proxima" {
    count = 1

    constraint {
      attribute = "${node.datacenter}"
      operator = "="
      value = "proxima"
    }

    network {
      mode = "bridge"
    }

    service {
      name = "metrics-consul"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }

        sidecar_task {
          name = "connect-proxy-metrics-consul"
          #      "connect-gateway-<service>" when used as a gateway

          driver = "docker"

          config {
            image = "${meta.connect.sidecar_image}"
            #       "${meta.connect.gateway_image}" when used as a gateway

            args = [
              "-c",
              "${NOMAD_SECRETS_DIR}/envoy_bootstrap.json",
              "-l",
              "debug",
              "--concurrency",
              "${meta.connect.proxy_concurrency}",
              "--disable-hot-restart"
            ]
          }

          logs {
            max_files     = 2
            max_file_size = 2
          }

          resources {
            cpu    = 250
            memory = 128
          }

          shutdown_delay = "5s"
        }

      }
    }

    task "telegraf" {
      driver = "docker"

      config {
        image = "[[ .metricsImage ]]"

        auth {
          username = "[[ env "REGISTRY_USERNAME" ]]"
          password = "[[ env "REGISTRY_PASSWORD" ]]"
        }

        volumes = [
          "local/telegraf.conf:/etc/telegraf/telegraf.conf"
        ]
      }

      template {
        data = <<EOH
          TZ='Europe/Stockholm'

          {{ with secret "jobs/metrics-consul/shared/telegraf" }}
          INFLUX_TOKEN="{{ index .Data.data "INFLUX_TOKEN" }}"
          {{ end }}

          CONSUL_DATACENTER="proxima"
        EOH

        destination = "secrets/collector.env"
        env = true
        change_mode = "restart"
      }

      template {
        data = <<EOH
[[ fileContents "./config/telegraf.template.conf" ]]
        EOH

        destination = "local/telegraf.conf"
        change_mode = "restart"
      }
    }
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
